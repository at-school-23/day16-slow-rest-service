package com.example.simpleslowrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleSlowRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleSlowRestApplication.class, args);
	}

}
